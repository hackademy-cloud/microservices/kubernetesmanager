from typing import List, Optional, Tuple

import yaml
from dataclasses import dataclass
from kubernetes import client
from courseman.k8course.resources.deploy_course_config import CourseConfig
from courseman.k8course.resources.kubernetes_course import KubernetesCourse
from courseman.utils.model_utils import get_entry

from courseman.k8course.resources.deploy_course import DeployCourse


@dataclass
class MyLabel:
    key: str
    value: str


class KubernetesCourse:

    def __init__(self, course: DeployCourse, user_token: str, status: Optional[str] = None):
        self.course = course
        self.user_token = user_token
        self.status = status

    @staticmethod
    def namespace_prefix():
        return "lab-"

    def namespace(self) -> str:
        return "{}{}-{}".format(KubernetesCourse.namespace_prefix(),
                                self.course.id, self.user_token)

    def get_path(self):
        return "/{}-{}".format(self.user_token, self.course.id)


class KubernetesController:
    """
    Bei dem Deployment kann die Variable `{path}` benutzt werden, welche den Base Path
    der Anwendung enthält. Dieser Path ist eine Kombination aus User Token und Course id.
    Die Variable wird per `.format(...)` ersetzt.
    """

    def namespace_label(self) -> MyLabel:
        return MyLabel("environment", "deploy_course")

    def get_all(self):
        api = client.CoreV1Api()
        label = self.namespace_label()
        label_selector = "{}={}".format(label.key, label.value)
        resp = api.list_namespace(label_selector=label_selector)
        items = resp.items
        courses = []
        for item in items:
            metadata = item.metadata
            course_id = metadata.labels["course_id"]
            user_token = metadata.labels["user_token"]
            course = DeployCourse.get_or_none(course_id)
            if course is None:
                print("error reading deploy_course")
                # TODO: delete?
                continue
            course = KubernetesCourse(course, user_token)
            courses.append(course)
        return courses

    def _create_namespace(self, kc: KubernetesCourse):
        namespace_name = kc.namespace()
        label = self.namespace_label()
        label_selector_1 = "{}: '{}'".format(label.key, label.value)
        label_selector_2 = "{}: '{}'".format("course_id", kc.course.id)
        label_selector_3 = "{}: '{}'".format("user_token", kc.user_token)
        namespace_yml = "apiVersion: v1"
        namespace_yml += "\nkind: Namespace"
        namespace_yml += "\nmetadata:"
        namespace_yml += "\n  name: {}"
        namespace_yml += "\n  labels:"
        namespace_yml += "\n    {}"
        namespace_yml += "\n    {}"
        namespace_yml += "\n    {}"
        namespace_yml = namespace_yml.format(namespace_name,
                                             label_selector_1,
                                             label_selector_2,
                                             label_selector_3)
        namespace = yaml.safe_load(namespace_yml)
        api = client.CoreV1Api()
        resp = api.create_namespace(body=namespace)
        return resp

    def _start_deploy(self, kc: KubernetesCourse):
        path = kc.get_path()
        deployment_yml = kc.course.deployment
        deployment_yml = deployment_yml.format(path=path)
        deployment = yaml.safe_load(deployment_yml)
        api = client.AppsV1Api()
        namespace = kc.namespace()
        resp = api.create_namespaced_deployment(body=deployment,
                                                namespace=namespace)
        return resp

    def _start_svc(self, kc: KubernetesCourse):
        service_yml = kc.course.service
        service = yaml.safe_load(service_yml)
        api = client.CoreV1Api()
        namespace = kc.namespace()
        resp = api.create_namespaced_service(body=service, namespace=namespace)
        return resp

    def _start_ing(self, kc: KubernetesCourse, service_name: str):
        course_config = get_entry(CourseConfig)
        ingress_yml = course_config.default_ingress
        host = course_config.default_host
        path = kc.get_path()
        port = course_config.default_port
        ingress_yml = ingress_yml.format(host=host, path=path,
                                         service_name=service_name,
                                         service_port=port)
        ingress = yaml.safe_load(ingress_yml)
        api = client.ExtensionsV1beta1Api()
        namespace = kc.namespace()
        resp = api.create_namespaced_ingress(body=ingress, namespace=namespace)
        return resp

    def _start_secret(self, kc: KubernetesCourse):
        secret_yml = kc.course.secret
        secret = yaml.safe_load(secret_yml)
        api = client.CoreV1Api()
        namespace = kc.namespace()
        resp = api.create_namespaced_secret(body=secret, namespace=namespace)
        return resp

    def start(self, kc: KubernetesCourse):
        self._create_namespace(kc)
        self._start_secret(kc)
        self._start_deploy(kc)
        service = self._start_svc(kc)
        service_name = service.metadata.name
        self._start_ing(kc, service_name)
        kc.status = None

    def delete_namespace(self, kc: KubernetesCourse):
        api = client.CoreV1Api()
        name = kc.namespace()
        api.delete_namespace(name=name)


class KubernetesCourseList:
    def __init__(self):
        self.courses: List['KubernetesCourse'] = []
        self.k8ctrl = KubernetesController()

    def get_or_none(self, course: DeployCourse, user_token: str):
        for k8course in self.courses:
            if k8course.course.id == course.id and k8course.user_token == user_token:
                return k8course
        return None

    def update_cache(self):
        self.courses = self.k8ctrl.get_all()

    def get_all(self):
        return self.courses

    def get_all_from_user(self, user_token: str):
        k8courses = []
        for k8course in self.courses:
            if k8course.user_token == user_token:
                k8courses.append(k8courses)
        return k8courses

    def start_or_get(self, course: DeployCourse, user_token: str) -> Tuple[KubernetesCourse, bool]:
        """Starts a new deploy_course or returns the already started one.

        The boolean is true if the deploy_course is newly created, false otherwise.
        """
        kc = self.get_or_none(course=course, user_token=user_token)
        if kc is not None:
            print("Already started")
            return kc, False
        print("Starting")
        kc = KubernetesCourse.create(course=course, user_token=user_token)
        self.k8ctrl.start(kc)
        self.courses.append(kc)
        return kc, True

    def delete(self, course: DeployCourse, user_token: str) -> bool:
        kc = self.get_or_none(course=course, user_token=user_token)
        if kc is None:
            return False
        self.k8ctrl.delete_namespace(kc)
        self.courses.remove(kc)
        return False
