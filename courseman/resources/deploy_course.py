from peewee import Model, CharField, TextField, AutoField

from courseman.database import register_model


@register_model
class DeployCourse(Model):
    id = AutoField()
    name = CharField(null=False, unique=True)
    deployment = TextField(null=True)
    service = TextField(null=True)
    ingress = TextField(null=True)  # XXX currently not supported
    secret = TextField(null=True)
