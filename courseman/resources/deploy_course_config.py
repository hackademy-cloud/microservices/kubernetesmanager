from peewee import Model, CharField, TextField, AutoField

from courseman.database import register_model


default_ingress = """apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: user-enumeration-openssh-ingress
spec:
  rules:
  - host: {host}
    http:
      paths:
      - path: {path}
        backend:
          serviceName: {service_name}
          servicePort: {service_port}
"""


@register_model
class CourseConfig(Model):
    default_host = TextField(null=False, default="kube.localhost")
    default_port = TextField(null=False, default="http")
    default_ingress = TextField(null=False, default=default_ingress)
