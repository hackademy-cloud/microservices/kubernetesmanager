from flask import abort
from flask_restful import Resource, reqparse

from kubernetes import config as kubernetes_config

from courseman.k8course.resources.kubernetes_course import KubernetesCourseList
from courseman.k8course.resources.deploy_course import DeployCourse
from courseman.environment import api, ma
from courseman.utils.utils import get_or_abort


class K8CourseSchema(ma.Schema):
    class Meta:
        fields = ("deploy_course", "status")


k8course_schema = K8CourseSchema()
k8courses_schema = K8CourseSchema(many=True)


class K8CourseListAPI(Resource):
    def __init__(self, kubernetes_course_list: KubernetesCourseList):
        self.kubernetes_course_list = kubernetes_course_list
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_token', type=str, help='Specific User Token')

    def get(self):
        args = self.parser.parse_args()
        user_token = args["user_token"]
        if user_token is None:
            # XXX Permissions?
            k8courses = self.kubernetes_course_list.get_all()
            return k8courses_schema.dump(k8courses)
        k8courses = self.kubernetes_course_list.get_all_from_user(user_token)
        return k8courses_schema.dump(k8courses)


class K8CourseAPI(Resource):
    def __init__(self, kubernetes_course_list: KubernetesCourseList):
        self.kubernetes_course_list = kubernetes_course_list
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_token', required=True, type=str, help='Specific User Token')

    def get(self, course_id: int):
        course = get_or_abort(DeployCourse, id=course_id)
        args = self.parser.parse_args()
        kc = self.kubernetes_course_list.get_or_none(course, args['user_token'])
        return k8course_schema.dump(kc)

    def post(self, course_id: int):
        course = get_or_abort(DeployCourse, id=course_id)
        args = self.parser.parse_args()
        kc, started = self.kubernetes_course_list.start_or_get(course, args['user_token'])
        if started:
            return k8course_schema.dump(kc), 201
        return k8course_schema.dump(kc), 200

    def delete(self, course_id: int):
        course = get_or_abort(DeployCourse, id=course_id)
        args = self.parser.parse_args()
        deleted = self.kubernetes_course_list.delete(course, args['user_token'])
        if not deleted:
            abort(404)
        return None, 204


def init():
    kubernetes_config.load_kube_config()
    kubernetes_course_list = KubernetesCourseList()
    kubernetes_course_list.update_cache()
    api.add_resource(K8CourseAPI, '/k8course',                 endpoint = 'k8courses', resource_class_args=kubernetes_course_list)
    api.add_resource(K8CourseAPI, '/k8course/<int:course_id>', endpoint = 'k8course',  resource_class_args=kubernetes_course_list)
