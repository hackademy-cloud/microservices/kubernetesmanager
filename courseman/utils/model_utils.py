from peewee import Model


def get_entry(model: Model):
    for item in model.select():
        return item
    return model.create()