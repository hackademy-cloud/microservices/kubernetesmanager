from flask import abort
from flask_restful import reqparse, Resource
from courseman.environment import ma, api
from courseman.utils.utils import get_or_abort, setattr_parser
from courseman.k8course.resources.deploy_course import DeployCourse


class ShortCourseSchema(ma.Schema):
    class Meta:
        fields = ("id", "name")


class CourseSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "deployment", "service", "secret")


short_course_schema = ShortCourseSchema()
short_courses_schema = ShortCourseSchema(many=True)
course_schema = CourseSchema()
courses_schema = CourseSchema(many=True)


def get_filtered_courses():
    parser = reqparse.RequestParser()
    parser.add_argument('name', type=str, help='The name of the deploy_course')
    args = parser.parse_args()
    name = args["name"]
    if name:  # filter by name
        course = get_or_abort(DeployCourse, name=name)
        return []
    else:
        return DeployCourse.select()


class CourseListAPI(Resource):
    def get(self):
        courses = get_filtered_courses()
        if courses is None:
            return []
        return courses_schema.dump(courses)

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', required=True, type=str, help='The name of the deploy_course')
        parser.add_argument('deployment', type=str, help='Kubernetes deployment yaml')
        parser.add_argument('service', type=str, help='Kubernetes service yaml')
        parser.add_argument('secret', type=str, help='Kubernetes secret yaml, needed for private repositories')
        args = parser.parse_args()
        name = args["name"]
        if DeployCourse.get_or_none(name=name) is not None:
            abort(422)
        course = DeployCourse.create(name=name)
        return course_schema.dump(course), 201


class CourseAPI(Resource):
    def get(self, course_id: int):
        # show the post with the given id, the id is an integer
        return get_or_abort(DeployCourse, course_schema, id=course_id)

    def patch(self, course_id: int):
        course = get_or_abort(DeployCourse, id=course_id)
        parser = reqparse.RequestParser()
        parser.add_argument('deployment', type=str, help='Kubernetes deployment yaml')
        parser.add_argument('service', type=str, help='Kubernetes service yaml')
        parser.add_argument('secret', type=str, help='Kubernetes secret yaml, needed for private repositories')
        args = parser.parse_args()
        if setattr_parser(course, args):
            course.save()
            return None, 204
        abort(400)

    def delete(self, course_id: int):
        get_or_abort(DeployCourse, id=course_id)
        DeployCourse.delete().where(DeployCourse.id == course_id).execute()
        return None, 204


class SmallCourseListAPI(Resource):
    def get(self):
        courses = get_filtered_courses()
        if courses is None:
            return []
        return short_courses_schema.dump(courses)


class SmallCourseAPI(Resource):
    def get(self, course_id: int):
        course = get_or_abort(DeployCourse, id=course_id)
        return short_course_schema.dump(course)


def init():
    api.add_resource(CourseListAPI,         '/deploy_course',                          endpoint = 'courses')
    api.add_resource(CourseAPI,             '/deploy_course/<int:course_id>',          endpoint = 'deploy_course')
    api.add_resource(SmallCourseListAPI,    '/small_course',                    endpoint = 'small_courses')
    api.add_resource(SmallCourseAPI,        '/small_course/<int:course_id>',    endpoint = 'small_course')
