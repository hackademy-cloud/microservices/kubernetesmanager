from lods_lib import LodsApiClient

import courseman.resources  # needed for peewee
from .environment import app
from .errorhandling import add_errorhandlers
from courseman.database import Database, Schema
from .config import Config
from courseman.apis.user_api import init as user_api_init
from courseman.apis.company_api import init as company_api_init
from courseman.apis.series_api import init as series_api_init
from courseman.apis.course_api import init as course_api_init
from .utils.debug import enable_debug_output


def load_apis(lods_client: LodsApiClient):
    user_api_init()
    company_api_init()
    series_api_init(lods_client)
    course_api_init(lods_client)


def main():
    global lods_client
    enable_debug_output()
    Database.initialize_default()
    Schema.create_tables()
    my_config = Config.get().args_config
    lods_client = LodsApiClient(my_config.lods_api_key)

    add_errorhandlers()
    load_apis(lods_client)
    app.run(host=my_config.flask_host, port=my_config.flask_port)
