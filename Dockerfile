FROM python:3.8-alpine

WORKDIR /app

COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt && rm /requirements.txt

COPY . /app

VOLUME /volume

EXPOSE 80

CMD [ "python", "/app/run", "--db-file=/volume/database.sqlite", "--flask-host=0.0.0.0", "--flask-port=80"]
